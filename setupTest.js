const mongoose = require('mongoose');
require('dotenv').config({ path: './.env.test' });

beforeAll(async () => {
  const url = process.env.CONNECTION_STRING;
  await mongoose.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
});

afterAll(async () => {
  await mongoose.connection.close();
});