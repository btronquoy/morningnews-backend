const request = require('supertest');
const express = require('express');
const indexRouter = require('../routes/index'); // Adjust the path as necessary

const app = express();
app.use('/', indexRouter);


describe('GET /articles', () => {
  it('should return 200 OK', async () => {
    const res = await request(app).get('/articles');
    expect(res.statusCode).toEqual(200);
    expect(res.body).toHaveProperty('articles');
    expect(Array.isArray(res.body.articles)).toBe(true);
  });
});
