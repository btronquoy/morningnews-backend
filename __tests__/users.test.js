const request = require('supertest');
const express = require('express');
const userRouter = require('../routes/users'); // Adjust the path as necessary
const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const uid2 = require('uid2');
const User = require('../models/users');
const { checkBody } = require('../modules/checkBody');

jest.mock('bcrypt');
jest.mock('uid2');
jest.mock('../models/users');
jest.mock('../modules/checkBody');

const app = express();
app.use(express.json());
app.use('/', userRouter);

describe('User API', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('POST /signup', () => {
    it('should create a new user', async () => {
      checkBody.mockReturnValue(true);
      User.findOne.mockResolvedValue(null);
      bcrypt.hashSync.mockReturnValue('hashedpassword');
      uid2.mockReturnValue('token');
      User.prototype.save = jest.fn().mockResolvedValue({ token: 'token' });

      const res = await request(app)
        .post('/signup')
        .send({ username: 'testuser', password: 'testpassword' });

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('result', true);
      expect(res.body).toHaveProperty('token');
      expect(User.findOne).toHaveBeenCalledWith({ username: 'testuser' });
      expect(bcrypt.hashSync).toHaveBeenCalledWith('testpassword', 10);
      expect(User.prototype.save).toHaveBeenCalled();
    });

    it('should not create user if username already exists', async () => {
      checkBody.mockReturnValue(true);
      User.findOne.mockResolvedValue({ username: 'testuser' });

      const res = await request(app)
        .post('/signup')
        .send({ username: 'testuser', password: 'testpassword' });

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('result', false);
      expect(res.body).toHaveProperty('error', 'User already exists');
      expect(User.findOne).toHaveBeenCalledWith({ username: 'testuser' });
    });

    it('should return error if required fields are missing', async () => {
      checkBody.mockReturnValue(false);

      const res = await request(app)
        .post('/signup')
        .send({ username: 'testuser' });

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('result', false);
      expect(res.body).toHaveProperty('error', 'Missing or empty fields');
      expect(checkBody).toHaveBeenCalledWith({ username: 'testuser' }, ['username', 'password']);
    });
  });

  describe('POST /signin', () => {
    it('should sign in an existing user', async () => {
      checkBody.mockReturnValue(true);
      User.findOne.mockResolvedValue({
        username: 'testuser',
        password: 'hashedpassword',
        token: 'token',
      });
      bcrypt.compareSync.mockReturnValue(true);

      const res = await request(app)
        .post('/signin')
        .send({ username: 'testuser', password: 'testpassword' });

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('result', true);
      expect(res.body).toHaveProperty('token');
      expect(User.findOne).toHaveBeenCalledWith({ username: 'testuser' });
      expect(bcrypt.compareSync).toHaveBeenCalledWith('testpassword', 'hashedpassword');
    });

    it('should return error if user not found or wrong password', async () => {
      checkBody.mockReturnValue(true);
      User.findOne.mockResolvedValue({
        username: 'testuser',
        password: 'hashedpassword',
        token: 'token',
      });
      bcrypt.compareSync.mockReturnValue(false);

      const res = await request(app)
        .post('/signin')
        .send({ username: 'testuser', password: 'wrongpassword' });

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('result', false);
      expect(res.body).toHaveProperty('error', 'User not found or wrong password');
      expect(User.findOne).toHaveBeenCalledWith({ username: 'testuser' });
      expect(bcrypt.compareSync).toHaveBeenCalledWith('wrongpassword', 'hashedpassword');
    });

    it('should return error if required fields are missing', async () => {
      checkBody.mockReturnValue(false);

      const res = await request(app)
        .post('/signin')
        .send({ username: 'testuser' });

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('result', false);
      expect(res.body).toHaveProperty('error', 'Missing or empty fields');
      expect(checkBody).toHaveBeenCalledWith({ username: 'testuser' }, ['username', 'password']);
    });
  });

  describe('GET /canBookmark/:token', () => {
    it('should return if user can bookmark', async () => {
      User.findOne.mockResolvedValue({
        token: 'token',
        canBookmark: true,
      });

      const res = await request(app).get('/canBookmark/token');

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('result', true);
      expect(res.body).toHaveProperty('canBookmark', true);
      expect(User.findOne).toHaveBeenCalledWith({ token: 'token' });
    });

    it('should return error if user is not found', async () => {
      User.findOne.mockResolvedValue(null);

      const res = await request(app).get('/canBookmark/invalidtoken');

      expect(res.statusCode).toEqual(200);
      expect(res.body).toHaveProperty('result', false);
      expect(res.body).toHaveProperty('error', 'User not found');
      expect(User.findOne).toHaveBeenCalledWith({ token: 'invalidtoken' });
    });
  });
});
